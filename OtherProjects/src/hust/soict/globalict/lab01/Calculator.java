package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

public class Calculator {
    public static void main(String[] args) {
        double num1;
        String strNum1;
        double num2;
        String strNum2;

        strNum1 = JOptionPane.showInputDialog(null, 
        "Please input the first number: ", "Input the first number",
        JOptionPane.INFORMATION_MESSAGE);

        num1 = Double.parseDouble(strNum1);

        strNum2 = JOptionPane.showInputDialog(null, 
        "Please input the second number: ", "Input the second number",
        JOptionPane.INFORMATION_MESSAGE);

        num2 = Double.parseDouble(strNum2);

        String result = "Sum : " + (num1 + num2) + "\nDifference: " + (num1 - num2) + "\nProduct: " + (num1 * num2) + "\nQuotient: " + (num1/num2);

        JOptionPane.showMessageDialog(null, result, "Result", JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}
