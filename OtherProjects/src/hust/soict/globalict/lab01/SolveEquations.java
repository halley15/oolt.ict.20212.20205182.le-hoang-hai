package hust.soict.globalict.lab01;

import java.lang.Math;
import javax.swing.JOptionPane;

public class SolveEquations {
    public static void main(String[] args){
        double[] linear_data = new double[2];
        double[] system_data = new double[6];
        double[] sec_deg_data = new double[3];
        String linear_title = "First-degree Equation";
        String system_title = "First-degree System Equation";
        String sec_deg_title = "Second-degree Equation";
        String linear_des = "a1x + a2 = 0 ( a ≠ 0 )";
        String system_des = "a1x1 + a2x2 = a3\na4x1 + a5x2 = a6";
        String sec_deg_des = "a1x^2 + a2x + a3 =  0";
        //solve linear equation
        for(int i = 0; i < 2;i++){
            String input = JOptionPane.showInputDialog(null,
                            linear_des + "\nInput a" + (i+1),
                            linear_title,
                            JOptionPane.INFORMATION_MESSAGE);
            linear_data[i] = Double.parseDouble(input);
        }
        JOptionPane.showMessageDialog(null,
                                        "Solution: " + (-linear_data[1]/linear_data[0]),
                                        linear_title,
                                        JOptionPane.INFORMATION_MESSAGE);

        //solve linear system
        for(int i = 0; i < 6;i++){
            String input = JOptionPane.showInputDialog(null,
                    system_des + "\nInput a" + (i+1),
                    system_title,
                    JOptionPane.INFORMATION_MESSAGE);
            system_data[i] = Double.parseDouble(input);
        }
        double D = system_data[0]*system_data[4] - system_data[1]*system_data[3];
        double D1 = system_data[2]*system_data[4] - system_data[5]*system_data[1];
        double D2 = system_data[0]*system_data[5] - system_data[2]*system_data[3];
        if(D != 0){
            double x1 = D1/D;
            double x2 =  D2/D;
            JOptionPane.showMessageDialog(null,
                                            "The system has a unique solution: " + "x1 = " + x1 + ", " +
                                                    "x2 = " + x2,
                                            system_title,
                                            JOptionPane.INFORMATION_MESSAGE);
        }else if(D1 == 0 && D2 == 0){
            JOptionPane.showMessageDialog(null,
                                            "The system has infinitely many solutions",
                                            system_title,
                                            JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null,
                                            "The system has no solution",
                                            system_title,
                                            JOptionPane.INFORMATION_MESSAGE);
        }

        //second-degree equation
        for(int i = 0; i < 3;i++){
            String input = JOptionPane.showInputDialog(null,
                    sec_deg_des + "\nInput a" + (i+1),
                    sec_deg_title,
                    JOptionPane.INFORMATION_MESSAGE);
            sec_deg_data[i] = Double.parseDouble(input);
        }

        double delta = sec_deg_data[1] * sec_deg_data[1] - 4 * sec_deg_data[0] * sec_deg_data[2];
        if(delta == 0){
            double root = -sec_deg_data[1]/(2 * sec_deg_data[0]);
            JOptionPane.showMessageDialog(null,
                                            "The equation has double root: " + root,
                                            sec_deg_title,
                                            JOptionPane.INFORMATION_MESSAGE);
        }else if(delta > 0){
            double root1 = (- sec_deg_data[1] + Math.sqrt(delta))/(2 * sec_deg_data[0]);
            double root2 = (- sec_deg_data[1] - Math.sqrt(delta))/(2 * sec_deg_data[0]);
            JOptionPane.showMessageDialog(null,
                                            "The equation has two distinct roots: " + root1 + " and " + root2,
                                            sec_deg_title,
                                            JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null,
                                            "The equation has no solution",
                                            sec_deg_title,
                                            JOptionPane.INFORMATION_MESSAGE);
        }
    }
}