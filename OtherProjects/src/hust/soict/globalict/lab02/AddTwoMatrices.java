package hust.soict.globalict.lab02;

import java.util.Scanner;
public class AddTwoMatrices {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter number of rows and columns of these 2 matrices: ");
        int x = keyboard.nextInt();
        int y = keyboard.nextInt();
        double[][] matrix1 = new double[x][y];
        double[][] matrix2 = new double[x][y];
        System.out.println("Enter matrix 1: ");
        for(int i = 0; i< x; i++) {
            for(int j = 0; j < y; j++) {
                matrix1[i][j] = keyboard.nextDouble();
            }
        }
        System.out.println("Enter matrix 2: ");
        for(int i = 0 ; i < x;i++) {
            for(int j = 0; j < y;j++) {
                matrix2[i][j] = keyboard.nextDouble();
            }
        }
        System.out.println("The sum of 2 matrices is: ");
        for(int i = 0; i < x;i++) {
            for(int j = 0;j < y;j++) {
                System.out.print(matrix1[i][j] + matrix2[i][j]);
            }
            System.out.print("\n");
        }
        keyboard.close();
    }
}
