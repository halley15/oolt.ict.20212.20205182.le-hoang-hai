package hust.soict.globalict.test.utils;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {
    public static void main(String[] args) {
        MyDate[] dates = new MyDate[3];
        MyDate date1 = new MyDate();
        MyDate date2 = new MyDate(15, 1, 2002);
        MyDate date3 = new MyDate("April 30th 2022");
        dates[0] = date1;
        dates[1] = date2;
        dates[2] = date3;
        date1.print();
        date2.print();
        date3.print();
        date3.accept();
        date3.print();
        date3.setDay(-1);
        date3.print();
        date3.setMonth(12);
        date3.print();
        date3.formattedPrint();
        DateUtils.sortDates(dates);
        for (int i = 0; i < 3; i++) {
            dates[i].formattedPrint();
        }
    }
}
