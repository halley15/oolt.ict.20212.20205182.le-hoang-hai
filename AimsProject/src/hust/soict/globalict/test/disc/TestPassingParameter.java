//package hust.soict.globalict.test.disc;
//
//import hust.soict.globalict.aims.media.DigitalVideoDisc;
//
//class ObjectWrapper {
//    DigitalVideoDisc object;
//    ObjectWrapper(DigitalVideoDisc object) {
//        this.object = object;
//    }
//}
//
//public class TestPassingParameter {
//    public static void main(String[] args) {
//        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
//        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
//        ObjectWrapper objWrapper1 = new ObjectWrapper(jungleDVD);
//        ObjectWrapper objectWrapper2 = new ObjectWrapper(cinderellaDVD);
//
//        swap(objWrapper1, objectWrapper2);
//        System.out.println("Jungle dvd title: " + objWrapper1.object.getTitle());
//        System.out.println("Cinderella dvd title: " + objectWrapper2.object.getTitle());
//
//        changeTitle(jungleDVD, cinderellaDVD.getTitle());
//        System.out.println("Jungle dvd title: " + jungleDVD.getTitle());
//    }
//
//    public static void swap(ObjectWrapper o1, ObjectWrapper o2) {
//        DigitalVideoDisc tmp = o1.object;
//        o1.object = o2.object;
//        o2.object = tmp;
//    }
//
//    public static void changeTitle(DigitalVideoDisc dvd, String title) {
//        String oldTitle = dvd.getTitle();
//        dvd.setTitle(title);
//        dvd = new DigitalVideoDisc(oldTitle);
//    }
//}
//
//
