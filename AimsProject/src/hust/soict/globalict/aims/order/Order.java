package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

import java.util.ArrayList;

class MaxNumberOrdersExceedException extends Exception {

}
public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;
    public static int nbOrders = 0;
    private final ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    private MyDate dateOrdered;

    public Order() {
        try {
            if (Order.nbOrders == MAX_LIMITED_ORDERS) {
                throw new MaxNumberOrdersExceedException();
            } else {
                this.dateOrdered = new MyDate();
                Order.nbOrders++;
            }
        } catch (MaxNumberOrdersExceedException ex) {
            System.out.println("Maximum number of orders reached!");
        }
    }

    public void addMedia(Media itemToAdd) {
        if (itemsOrdered.size() == MAX_NUMBERS_ORDERED) {
            System.out.println("Max number of items reached!");
        } else {
            itemsOrdered.add(itemToAdd);
            System.out.println(itemsOrdered.size());
            System.out.println("Item added successfully.");
        }
    }

    public void removeMedia(String id) {
        for (Media media : itemsOrdered) {
            if (media.getId().equals(id)) {
                itemsOrdered.remove(media);
                return;
            }
        }
        System.out.println("Item not found.");
    }
    public float totalCost() {
        float cost = 0;
        for (Media media : itemsOrdered) {
            cost += media.getCost();
        }
        return cost;
    }

    public void printOrder() {
        System.out.println("*************************Order*************************");
        System.out.println("Date: " + this.dateOrdered.getDay() + " - " + this.dateOrdered.getMonth() + " - " + this.dateOrdered.getYear());
        for (int i = 0; i < itemsOrdered.size(); i++) {
            Media item = itemsOrdered.get(i);
            System.out.println((i + 1) + ". " + item.getId() + " - " + item.getTitle() + " - " + item.getCategory() + " - " + item.getCost() + "$");
        }
        System.out.println("Total cost: " + this.totalCost());
        System.out.println("*******************************************************");
    }

    public Media getALuckyItem() {
        int luckyItemIndex = (int)(Math.random() * itemsOrdered.size());
        Media luckyItem = itemsOrdered.get(luckyItemIndex);
        removeMedia(luckyItem.getId());
        return luckyItem;
    }
}