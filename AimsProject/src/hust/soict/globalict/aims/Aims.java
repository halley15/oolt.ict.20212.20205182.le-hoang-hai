package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MemoryDaemon;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Aims {
    public static Order createNewOrder() {
        return new Order();
    }

    public static boolean askForPlaying() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Do you want to play the disc? (yes/no)");
        String choice = sc.nextLine();

        return choice.equals("yes");
    }

    public static void addItemToOrder(Order order) {
        Scanner sc = new Scanner(System.in);
        String type;
        String id;
        String title;
        String category;
        float cost;

        System.out.println("Enter media type: ");
        type = sc.nextLine();
        System.out.println(type);

        System.out.println("Enter id: ");
        id = sc.nextLine();

        System.out.println("Enter title: ");
        title = sc.nextLine();

        System.out.println("Enter category: ");
        category = sc.nextLine();
        System.out.println(category);

        System.out.println("Enter cost: ");
        cost = sc.nextFloat();
        System.out.println(cost);
        switch (type) {
            case "Book" -> {
                int number;
                Book book = new Book(id, title, category, cost);

                System.out.println("Enter the number of authors: ");
                number = sc.nextInt();

                for (int i = 0; i < number; i++) {
                    String author;

                    if (i == 0) {
                        sc.nextLine();
                    }

                    author = sc.nextLine();
                    book.addAuthor(author);

                }

                order.addMedia(book);
            }

            case "Compact Disc" -> {
                int numberOfTracks;
                String director;
                String artist;

                System.out.println("Enter director: ");
                director = sc.nextLine();

                System.out.println("Enter artist: ");
                artist = sc.nextLine();

                CompactDisc compactDisc = new CompactDisc(id, title, category, cost, 0, director, artist);
                System.out.println("Enter the number of tracks: ");
                numberOfTracks = sc.nextInt();
                for (int i = 1; i <= numberOfTracks; i++) {
                    String titleOfTrack;
                    int lengthOfTrack;

                    System.out.println("Enter the track " + i + " title: ");
                    titleOfTrack = sc.nextLine();

                    System.out.println("Enter the track " + i + " length: ");
                    lengthOfTrack = sc.nextInt();

                    Track track = new Track(titleOfTrack, lengthOfTrack);
                    compactDisc.addTrack(track);
                }

                order.addMedia(compactDisc);
                if (askForPlaying()) {
                    compactDisc.play();
                }
            }

            case "Digital Video Disc" -> {
                String director;
                int length;

                System.out.println("Enter the director: ");
                director = sc.nextLine();

                System.out.println("Enter length: ");
                length = sc.nextInt();

                DigitalVideoDisc disc = new DigitalVideoDisc(id, title, category, cost, director, length);

                order.addMedia(disc);
                if (askForPlaying()) {
                    disc.play();
                }
            }

            default -> System.out.println("Invalid media format.");
        }
        System.out.println("Sc will close");
        //sc.close();
        System.out.println("Sc closed");
    }

    public static void deleteItemById(Order order) {
        Scanner sc = new Scanner(System.in);
        String id;
        System.out.println("Enter id: ");
        id = sc.nextLine();
        order.removeMedia(id);
    }

    public static void displayItems(Order order) {
        order.printOrder();
    }
    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void main(String[] args) {
        int choice = -1;
        Order order = null;
        Scanner sc = new Scanner(System.in);
        MemoryDaemon memoryDaemon = new MemoryDaemon();
        Thread thread = new Thread(memoryDaemon);
        thread.setDaemon(true);
        thread.start();

        while(choice != 0) {
            showMenu();
            //System.out.println("TF: " + sc.hasNext("\n") + " " + sc.hasNext("") + " " + sc.hasNext("\t"));
            //choice = sc.nextInt();
            int n = sc.nextInt();
            choice = n;
            System.out.println(n);
//            try {
//                //System.out.println(sc.next());
//                String temp;
//                System.out.println("What is temp " + temp);
//                temp = sc.nextLine();
//                System.out.println("What is temp " + temp);
//                choice = Integer.parseInt(temp);
//                System.out.println("OK");
//            } catch (Exception error) {
//                System.out.println(choice);
//                while (sc.hasNext("\n")) {
//                    sc.next();
//                }
//                System.out.println("This is bad");
//                choice = sc.nextInt();
//            }


            switch (choice) {
                case 1:
                    order = createNewOrder();
                    break;
                case 2:
                    if (order != null) {
                        addItemToOrder(order);
                    } else {
                        System.out.println("Please create an order first.");
                    }
                    break;
                case 3:
                    if (order != null) {
                        deleteItemById(order);
                    } else {
                        System.out.println("Please create an order first");
                    }
                    break;
                case 4:
                    if (order != null) {
                        displayItems(order);
                    } else {
                        System.out.println("Please create an order first");
                    }
                    break;
                case 0:
                    break;
            }
        }
        sc.close();
    }

}
