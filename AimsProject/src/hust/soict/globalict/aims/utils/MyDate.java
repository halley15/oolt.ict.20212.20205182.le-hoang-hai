package hust.soict.globalict.aims.utils;

import javax.swing.JOptionPane;

public class MyDate {
    private int day;
    private int month;
    private int year;

    public MyDate() {
        String[] components = java.time.LocalDate.now().toString().split("-");
        this.day = Integer.parseInt(components[2]);
        this.month = Integer.parseInt(components[1]);
        this.year = Integer.parseInt(components[0]);
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String date) {
        String[] months = {"January", "February", "March", "April", "May",
                          "June", "July", "August", "September", "October", "November", "December"};
        String[] components = date.split("\\s");
        this.year = Integer.parseInt(components[2]);
        if(components[1].length() == 4) {
            this.day = Integer.parseInt(components[1].substring(0, 2));
        }else {
            this.day = Integer.parseInt(components[1].substring(0, 1));
        }
        for (int i = 0; i < months.length; i++) {
            if (months[i].equals(components[0])) {
                this.setMonth(i+1);
                break;
            }
        }
    }

    public MyDate(String day, String month, String year) {
        String[] months = {"January", "February", "March", "April", "May",
                "June", "July", "August", "September", "October", "November", "December"};
        for (int i = 0; i < 12; i++) {
            if (month.equals(months[i])) {
                this.setMonth(i + 1);
                break;
            }
        }
        String[] numericalOrder = {"first", "second", "third", "fourth",
                                "fifth", "sixth", "seventh", "eight", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth",
                                "sixteenth", "seventeenth", "eighteenth", "nineteenth"};
        String[] dayComponents = day.split("-");
        if (dayComponents.length == 1) {
            if (day.equals("twenty")) {
                this.setDay(20);
            } else if (day.equals("thirty")) {
                this.setDay(30);
            } else {
                for (int i = 0; i < 18; i++) {
                    if (day.equals(numericalOrder[i])) {
                        this.setDay(i+1);
                        break;
                    }
                }
            }
        } else {
            String dayString = "";
            if (dayComponents[0].equals("twenty")) {
                dayString += "2";
            } else {
                dayString += "3";
            }
            for (int i = 0; i < 10; i++) {
                if (dayComponents[1].equals(numericalOrder[i])) {
                    dayString += i + 1;
                    break;
                }
            }
            this.setDay(Integer.parseInt(dayString));
        }
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int day) {
        if(day > 0 && day <= 31) {
            this.day = day;
        }
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int month) {
        if(month > 0 && month <= 12) {
            this.month = month;
        }
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int year) {
        if(year > 0) {
            this.year = year;
        } 
    }

    public void accept() {
        String[] components = JOptionPane.showInputDialog(null, "day-month-year", "Please enter date", JOptionPane.INFORMATION_MESSAGE).split("-");
        setDay(Integer.parseInt(components[0]));
        setMonth(Integer.parseInt(components[1]));
        setYear(Integer.parseInt(components[2]));
    }

    public void print() {
        String[] months = {"January", "February", "March", "April", "May",
                "June", "July", "August", "September", "October", "November", "December"};
        String monthString = months[this.getMonth() - 1];
        String dayString = Integer.toString(this.getDay());
        char lastChar = dayString.charAt(dayString.length() - 1);
        if (lastChar == '1' && !dayString.equals("11")) {
            dayString += "st";
        } else if (lastChar == '2' && !dayString.equals("12")) {
            dayString += "nd";
        } else if (lastChar == '3' && !dayString.equals("13")) {
            dayString += "rd";
        } else {
            dayString += "th";
        }
        JOptionPane.showMessageDialog(null, monthString + " " + dayString + " " + this.getYear());
    }

    public void formattedPrint() {
        JOptionPane.showMessageDialog(null, getDay() + "/" + getMonth() + "/" + getYear());
    }
}


