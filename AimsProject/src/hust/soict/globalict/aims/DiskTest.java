//package hust.soict.globalict.aims;
//
//import hust.soict.globalict.aims.media.DigitalVideoDisc;
//import hust.soict.globalict.aims.order.Order;
//
//import java.util.Scanner;
//
//public class DiskTest {
//    public static void main(String[] args) {
//        Order anOrder1 = new Order();
//        Scanner sc = new Scanner(System.in);
//
//        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
//        dvd1.setCategory("Animation");
//        dvd1.setCost(19.95f);
//        dvd1.setDirector("Roger Allers");
//        dvd1.setLength(87);
//        anOrder1.addDigitalVideoDisc(dvd1);
//
//        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
//        dvd2.setCategory("Science Fiction");
//        dvd2.setCost(24.95f);
//        dvd2.setDirector("George Lucas");
//        dvd2.setLength(124);
//        anOrder1.addDigitalVideoDisc(dvd2);
//
//        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
//        dvd3.setCategory("Animation");
//        dvd3.setCost(18.99f);
//        dvd3.setDirector("John Musker");
//        dvd3.setLength(90);
//        anOrder1.addDigitalVideoDisc(dvd3);
//
//        System.out.print("Enter title: ");
//        String title = sc.nextLine();
//        System.out.println(dvd1.search(title));
//        System.out.println(anOrder1.totalCost());
//        DigitalVideoDisc luckyDisc = anOrder1.getALuckyItem();
//        System.out.println(luckyDisc.getTitle());
//        System.out.println(anOrder1.totalCost());
//
//        sc.close();
//    }
//}
